package ir.aut.ceit.app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Main {

    private static final String URL_OPEN_WEATHER_MAP_WEATHER_LONDON_UK =
            "http://api.openweathermap.org/data/2.5/weather?q=london&appid=02b9d7e3c9c46176bb59618c8485d395";

    public static void main(String[] args) {

        String result = "";
        try {
            URL urlWeather = new URL(URL_OPEN_WEATHER_MAP_WEATHER_LONDON_UK);
            HttpURLConnection httpURLConnection = (HttpURLConnection) urlWeather.openConnection();

            if (httpURLConnection.getResponseCode() == HttpURLConnection.HTTP_OK) {

                InputStreamReader inputStreamReader = new InputStreamReader(httpURLConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader, 8192);
                String line = null;
                while ((line = bufferedReader.readLine()) != null) {
                    result += line;
                }
                bufferedReader.close();

                String weatherResult = ParseResult(result);

                System.out.println(weatherResult);
            } else {
                System.out.println("Error in httpURLConnection.getResponseCode()!!!");
            }

        } catch (MalformedURLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    static private String ParseResult(String json) throws JSONException {

        JSONObject jsonObject = new JSONObject(json);

        //"coord"
        JSONObject jsonObjectCoord = jsonObject.getJSONObject("coord");
        Double resultLon = jsonObjectCoord.getDouble("lon");
        Double resultLat = jsonObjectCoord.getDouble("lat");

        //"sys"
        JSONObject jsonObjectSys = jsonObject.getJSONObject("sys");
        String resultCountry = jsonObjectSys.getString("country");
        int resultSunrise = jsonObjectSys.getInt("sunrise");
        int resultSunset = jsonObjectSys.getInt("sunset");

        //"weather"
        String result_weather;
        JSONArray jsonArrayWeather = jsonObject.getJSONArray("weather");
        if (jsonArrayWeather.length() > 0) {
            JSONObject jsonObjectWeather = jsonArrayWeather.getJSONObject(0);
            int resultId = jsonObjectWeather.getInt("id");
            String resultMain = jsonObjectWeather.getString("main");
            String resultDescription = jsonObjectWeather.getString("description");
            String resultIcon = jsonObjectWeather.getString("icon");

            result_weather ="\tdescription: " + resultDescription ;
        } else {
            result_weather = "weather empty!";
        }

        //"base"
        String resultBase = jsonObject.getString("base");

        //"main"
        JSONObject jsonObjectMain = jsonObject.getJSONObject("main");
        Double resultTemp = jsonObjectMain.getDouble("temp");
        Double resultPressure = jsonObjectMain.getDouble("pressure");
        Double resultHumidity = jsonObjectMain.getDouble("humidity");
        Double resultTempMin = jsonObjectMain.getDouble("temp_min");

        //"wind"
        JSONObject jsonObjectWind = jsonObject.getJSONObject("wind");
        Double resultSpeed = jsonObjectWind.getDouble("speed");
//        Double resultGust = jsonObjectWind.getDouble("gust");
        Double resultDeg = jsonObjectWind.getDouble("deg");
        String resultWind = "wind speed: " + resultSpeed + "\tdeg: " + resultDeg;

        //"clouds"
        JSONObject jsonObjectClouds = jsonObject.getJSONObject("clouds");
        int resultAll = jsonObjectClouds.getInt("all");

        //"dt"\
        int resultDt = jsonObject.getInt("dt");

        //"id"
        int resultId = jsonObject.getInt("id");

        //"name"
        String resultName = jsonObject.getString("name");

        //"cod"
        int resultCod = jsonObject.getInt("cod");

        return
                "\tlon: " + resultLon + "\tlat: " + resultLat + "\n" +
                        result_weather + "\n" +
                       "humidity: " + resultHumidity +"\n"+
                        ""+resultWind + "\n" +
                        "name: " + resultName + "\n" +
                        "\n";
    }

}